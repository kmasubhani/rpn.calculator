/**
 * 
 */
package com.realpage.assessment.rpn.calculator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.realpage.assessment.rpn.calculator.exception.RpnException;
import com.realpage.assessment.rpn.calculator.logic.RpnCalculator;

/**
 * Test cases for RPN calculator
 * 
 * @author bhanu chiguluri
 *
 */
@RunWith(JUnit4.class)
public class RpnCalculatorTest {
	
	
	private RpnCalculator rpnCalculator;
	
	public RpnCalculatorTest() {
		rpnCalculator = RpnCalculator.getInstance();
	}
	
	@Before
	public void init() {
		rpnCalculator.clear();
	}
	
	@Test
	public void testScenario1() throws RpnException {
		Double value1 = rpnCalculator.push("5");
		assertTrue(value1==5.0d);
		Double value2 = rpnCalculator.push("8");
		assertTrue(value2==8.0d);
		Double value3 = rpnCalculator.push("+");
		assertTrue(value3==13.0d);
	}
	
	@Test
	public void testScenario2() throws RpnException {
		Double value1 = rpnCalculator.push("5 8 +");
		assertTrue(value1==13.0d);
		Double value2 = rpnCalculator.push("13 -");
		assertTrue(value2==0.0d);
	}
	
	@Test
	public void testScenario3() throws RpnException {
		Double value1 = rpnCalculator.push("-3");
		assertTrue(value1==-3.0d);
		Double value2 = rpnCalculator.push("-2");
		assertTrue(value2==-2.0d);
		Double value3 = rpnCalculator.push("*");
		assertTrue(value3==6.0d);
		Double value4 = rpnCalculator.push("5");
		assertTrue(value4==5.0d);
		Double value5 = rpnCalculator.push("+");
		assertTrue(value5==11.0d);
	}
	
	@Test
	public void testScenario4() throws RpnException{
		Double value1 = rpnCalculator.push("5");
		assertTrue(value1==5.0d);
		Double value2 = rpnCalculator.push("9");
		assertTrue(value2==9.0d);
		Double value3 = rpnCalculator.push("1");
		assertTrue(value3==1.0d);
		Double value4 = rpnCalculator.push("-");
		assertTrue(value4==8.0d);
		Double value5 = rpnCalculator.push("/");
		assertTrue(value5==0.625d);
	}
	
	@Test(expected = RpnException.class)
	public void testErrorScenario1() throws RpnException {
		Double value1 = rpnCalculator.push("AB 5 +");
	}
	
	@Test(expected = RpnException.class)
	public void testErrorScenario2() throws RpnException {
		Double value1 = rpnCalculator.push("5 0 /");
	}
	
	@Test(expected = RpnException.class)
	public void testErrorScenario3() throws RpnException {
		Double value1 = rpnCalculator.push("1 2 / +");
	}
	
	@After
	public void tearDown() {
		
	}

}
