package com.realpage.assessment.rpn.calculator.logic;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import com.realpage.assessment.rpn.calculator.constants.Constants;
import com.realpage.assessment.rpn.calculator.exception.RpnException;

/**
 * The core class of the application that takes care of the business logic
 * 
 * @author bhanu chiguluri
 *
 */
public class RpnCalculator {

	// singleton instance
	private static RpnCalculator instance;

	// stack to consume numbers
	private Stack<Double> theStack;

	// queue to consume the results of arithemtic operations
	private Queue<Double> theQueue;

	public static RpnCalculator getInstance() {
		if (instance == null) {
			instance = new RpnCalculator();
		}
		return instance;
	}

	private RpnCalculator() {

		this.theStack = new Stack<Double>();
		this.theQueue = new LinkedList<Double>();

	}

	/**
	 * Entry point for numbers and/or arithmetic operations to the logic
	 * Consumes the input and directs it to the stack or queue accordingly
	 * @param input
	 * @return
	 * @throws RpnException
	 */
	public Double push(String input) throws RpnException {

		Double response = null;

		String[] inputArray = input.split(" ");
		boolean isMultipleInput = inputArray.length > 1;

		// if there are multiple inputs in a single line, parse and process them one-by-one
		if (isMultipleInput) {
			List<String> inputList = Arrays.asList(inputArray);
			for (String inputString : inputList) {
				if (isArithmetic(inputString)) {
					response = calculate(inputString);
				} else {
					response = pushNumber(inputString);
				}
			}
		}
		// if single input, redirect as needed i.e. if it's a number push to the stack, if it's an operator, perform the calculation
		else {
			if (isArithmetic(input)) {
				response = calculate(input);
			} else {
				response = pushNumber(input);
			}
		}

		return response;

	}
	
	/**
	 * Clears the stack and queues. Called when something goes wrong for restart
	 */
	public void clear() {
		theStack.clear();
		theQueue.clear();
	}

	/**
	 * Pushes the number to the stack
	 * If it's not a number, throws an exception
	 * @param number
	 * @return
	 * @throws RpnException
	 */
	private Double pushNumber(String number) throws RpnException {
		try {
			return theStack.push(Double.valueOf(number));
		} catch (NumberFormatException e) {
			clear();
			throw new RpnException("Invalid number. Please start again!");
		}
	}

	/**
	 * If the input is an arithmetic operator, performs the calculation
	 * @param arithmeticOperator
	 * @return
	 * @throws RpnException
	 */
	private Double calculate(String arithmeticOperator) throws RpnException {

		try {
			Double result = null;

			Double existingValue = theQueue.poll();

			if (existingValue == null) {
				Double value1 = theStack.pop();
				Double value2 = theStack.pop();
				result = doArithmetic(value1, value2, arithmeticOperator);
			} else {
				Double value2 = theStack.pop();
				result = doArithmetic(existingValue, value2, arithmeticOperator);
			}

			theQueue.add(result);

			return result;
		} catch (EmptyStackException e) {
			throw new RpnException("Invalid notation. Please start again.");
		}
	}

	/**
	 * Perform the arithmetic operation using the given values
	 * Fails when dividing by zero and starts over 
	 * 
	 * @param value1
	 * @param value2
	 * @param arithmeticOperator
	 * @return
	 * @throws RpnException
	 */
	private Double doArithmetic(Double value1, Double value2, String arithmeticOperator) throws RpnException {

		Double result = null;

		try {
			if (Constants.ADD.equals(arithmeticOperator)) {
				result = value1 + value2;
			} else if (Constants.SUBTRACT.equals(arithmeticOperator)) {
				result = value2 - value1;
			} else if (Constants.MULTIPLY.equals(arithmeticOperator)) {
				result = value2 * value1;
			} else if (Constants.DIVIDE.equals(arithmeticOperator)) {
				if (value1==0.0d) {
					throw new RpnException("Division by zero?!! Please start again.");
				}
				result = value2 / value1;
			}
		} catch (IllegalStateException e) {
			clear();
			System.out.println("Something went wrong. Please try again.");
		}

		return result;
	}

	/**
	 * Returns true if the input is an arithmetic operator
	 * 
	 * @param input
	 * @return
	 */
	private boolean isArithmetic(String input) {
		boolean result = Constants.ADD.equals(input) || Constants.SUBTRACT.equals(input)
				|| Constants.MULTIPLY.equals(input) || Constants.DIVIDE.equals(input);
		return result;
	}

}
