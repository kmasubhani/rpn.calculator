package com.realpage.assessment.rpn.calculator.exception;

public class RpnException extends Exception {
	
	public RpnException(String message) {
		super(message);
	}

}
