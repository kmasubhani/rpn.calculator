package com.realpage.assessment.rpn.calculator;

import java.util.Scanner;

import com.realpage.assessment.rpn.calculator.constants.Constants;
import com.realpage.assessment.rpn.calculator.exception.RpnException;
import com.realpage.assessment.rpn.calculator.logic.RpnCalculator;

/**
 * Reverse Polish Notation calculator using Stacks and Queues
 * 
 * @author Bhanu Chiguluri June 12, 2018
 * @version 1.0
 *
 */
public class App {
	private static RpnCalculator instance;

	public App() {
		instance = RpnCalculator.getInstance();
	}

	public static void main(String[] args) {
		System.out.println("***Reverse Polish Notation Calculator***");
		System.out.println("** written by Bhanu Chiguluri **");

		// initialize
		App app = new App();

		System.out.println("Enter your input :: ");

		Scanner scanner = new Scanner(System.in);

		while (scanner.hasNext()) {

			String input = scanner.nextLine();

			// EXIT CONDITION
			if (Constants.QUIT.equalsIgnoreCase(input)) {
				System.out.println("Thank you. Bye! :)");
				System.exit(0);
			} else {
				String response;
				try {
					response = instance.push(input).toString();
				} catch (RpnException e) {
					response = e.getMessage();
				}
				System.out.println(response);
			}
		}
	}
}
